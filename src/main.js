var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var size = 29
var startSize = 23
var rule = [5, 2, 6, 8]
var limit = 4
var stack = []
// var growth = true
var array = create2DArray(size, size, 0, true)
stack.push(array)
var startArray = create2DArray(startSize, startSize, 0, false)
for (var i = 0; i < startArray.length; i++) {
  for (var j = 0; j < startArray[i].length; j++) {
    if (startArray[i][j] === 1) {
      array[i + Math.floor((array.length - startSize) * 0.5)][j + Math.floor((array.length - startSize) * 0.5)] = 1
    }
  }
}
stack.push(array)
var checkArray = create2DArray(size, size, false, true)
var frames = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var k = stack.length - 1; k > 0; k--) {
    for (var i = 0; i < stack[k].length; i++) {
      for (var j = 0; j < stack[k][i].length; j++) {
        noStroke()
        if (stack[k][i][j] === 1) {
          fill(255 * (k / limit))
        } else if (stack[k][i][j] === 0) {
          noFill()
        }
        rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (15 / size), (46 / 768) * boardSize * (15 / size) * ((k + 1) / limit), (46 / 768) * boardSize * (15 / size) * ((k + 1) / limit))
      }
    }

  }

  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0
    array = getNeighbours(array, rule)
    for (var i = 0; i < size; i++) {
      array[Math.floor(Math.random() * (array.length))][Math.floor(Math.random() * (array.length))] = 1
    }
    stack.push(array)

    if (stack.length > limit) {
      stack = stack.splice(1)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 2)
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(array, rule) {
  var neighbours = create2DArray(size, size, 0, true)
  for (var x = 0; x < array.length; x++) {
    for (var y = 0; y < array[x].length; y++) {
      if (x < array.length - 1) {
        if (array[x + 1][y] === 1) {
          neighbours[x][y]++
        }
      }
      if (x > 1) {
        if (array[x - 1][y] === 1) {
          neighbours[x][y]++
        }
      }
      if (y < array.length - 1) {
        if (array[x][y + 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (y > 1) {
        if (array[x][y - 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x < array.length - 1 && y < array.length - 1) {
        if (array[x + 1][y + 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x < array.length - 1 && y > 1) {
        if (array[x + 1][y - 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x > 1 && y < array.length - 1) {
        if (array[x - 1][y + 1] === 1) {
          neighbours[x][y]++
        }
      }
      if (x > 1 && y > 1) {
        if (array[x - 1][y - 1] === 1) {
          neighbours[x][y]++
        }
      }
    }
  }
  var nextGeneration = create2DArray(size, size, 0, true)
  for (var i = 0; i < neighbours.length; i++) {
    for (var j = 0; j < neighbours[i].length; j++) {
      if (array[i][j] === 1) {
        if (neighbours[i][j] >= rule[1] && neighbours[i][j] <= rule[0]) {
          nextGeneration[i][j] = 1
        } else if (neighbours[i][j] > rule[0] || neighbours[i][j] === rule[3]) {
          nextGeneration[i][j] = 0
        }
      }
      if (array[i][j] === 0 && neighbours[i][j] === rule[2]) {
        nextGeneration[i][j] = 1
      }
    }
  }
  return nextGeneration
}
